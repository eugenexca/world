﻿$(function () {
    loadCountries();
    loadLanguages();
    $('#ddlCountries').change(loadCountry);
    $('#ddlLanguage').change(loadCountry);
})

// load countries list from WebApi to dropdown list
function loadCountries() {
    $.getJSON('/api/Country/', function (countries) {
        if (countries != null) {
            var ct = $('#ddlCountries');
            $.each(countries, function (index, country) {
                var op = new Option(country.Name, country.Id);
                ct.append($(op));
            })
        }
    })
}

function loadLanguages() {
    $.getJSON('/api/Language/', function (lgs) {
        if (lgs != null) {
            var ct = $('#ddlLanguage');
            $.each(lgs, function (key, language) {
                var op = new Option(language,key);
                ct.append($(op));
            })
        }
    })
}

// load country information for selected country and selected language.
function loadCountry() {
    var countryid = $('#ddlCountries').find(":selected").val();
    if (countryid > 0)
        $.getJSON('/api/Country/' + countryid, function (data) {
            showCountry(data);
        })
}

function showCountry(country) {
    if (country != null) {
        var language = $('#ddlLanguage').val();
        var countryname = country.Capital;
        $('#Capital').html(countryname);
        var bordername = "";
        $.each(country.Borders, function (i, c) {
            bordername += (c[""]);
            if (language != "")
                bordername += (" --- " + c[language]);
            bordername += "\r\n";
        });
        $('#Border').val(bordername);
    }
}