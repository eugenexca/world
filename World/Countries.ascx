﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Countries.ascx.cs" Inherits="World.Countries" %>
<div class="container">
    <div id="CountryList" class="CountryContent">
        <div class="row">
            <div class="col-md-6">
                <div class="CountryList form-group">
                    <label for="ddlCountries">Country:</label>
                    <asp:DropDownList ID="ddlCountries" runat="server" ClientIDMode="Static" CssClass="form-control" DataValueField="Name" DataTextField="Name" AutoPostBack="False"></asp:DropDownList>
                </div>

            </div>
        </div>
    </div>
    <div id="CountryDetail" class="CountryContent">
        <div class="row">
            <div class="col-md-12">
                <div class=" form-group">
                    <label for="Capital">Capital:</label>
                    <div id="Capital" class="form-control"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class=" form-group">
                    <label for="Border">Border:</label>
                    <textarea  id="Border" class="form-control" rows="10"></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="CountryList form-group">
                    <label for="ddlLanguage">Border Country Display Language:</label>
                    <asp:DropDownList ID="ddlLanguage" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
