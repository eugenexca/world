﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace World.DomainModel
{
    public class Country
    {
        public long id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "alpha3Code")]
        public string code { get; set; }
        public string capital { get; set; }
        public Dictionary<string, string> translations { get; set; }
        public string[] borders { get; set; }
    }
}