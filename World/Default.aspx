﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="World._Default" %>

<%@ Register Src="~/Countries.ascx" TagPrefix="uc1" TagName="Countries" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="page-header">
        <h3>World Countries</h3>
    </div>

    <uc1:Countries runat="server" ID="Countries" />
    <script src="Scripts/country.js"></script>
</asp:Content>
