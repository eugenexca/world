﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using World.DomainModel;
using World.Repository;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace World.Service
{
    public class CountryService : ICountryService
    {
        ICountryRepository repo = new CountryRepository();
        const int CacheExpireMins = 5;
        const string CountryCacheName = "Countries";

        public IList<Country> GetCountries()
        {
            ObjectCache cache = MemoryCache.Default;
            IList<Country> countries = cache[CountryCacheName] as IList<Country>;

            if (countries == null)
            {
                CacheItemPolicy policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = DateTime.Now.AddMinutes(CacheExpireMins);
                var ct = Task.Run(repo.GetCountries);
                countries = ct.Result;
                long countryid = 1;
                foreach (var c in countries)
                {
                    c.id = countryid++; //setup ID
                    c.translations.Add("",c.name); //add English name to dic
                }
                cache.Set(CountryCacheName, countries, policy);
            }

            return countries;
        }

        public Country GetCountry(int Id)
        {
            IList<Country> countries = GetCountries();
            return countries.FirstOrDefault(c => c.id == Id);
        }
    }
}