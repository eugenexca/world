﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using World.DomainModel;
using World.ViewModels;
using World.Service;
using World.Mapper;

namespace World.Controllers
{
    public class CountryController : ApiController
    {
        ICountryService svc = new CountryService();
        CountryMapper mapper = new CountryMapper();
        
        // GET: api/Country
        public IList<CountryListItemModel> Get()
        {
            Trace.WriteLine(string.Format("{0} --- WebApi called. GetCountries.",DateTime.Now));
            try { 
                return mapper.MapCountryListFrom(svc.GetCountries());
            }
            catch (Exception e)
            {
                Trace.WriteLine(string.Format("{0} --- Exception when GetCountries. Message = {1}, Stack={2}", DateTime.Now,e.Message, e.StackTrace));
                throw;
            }
        }

        // GET: api/Country/5
        public CountryViewModel Get(int Id)
        {
            Trace.WriteLine(string.Format("{0} --- WebApi called. GetCountry. Id = {1}",DateTime.Now ,Id) );
            try
            {
                return mapper.MapCountryViewFrom(svc.GetCountry(Id));
            }
            catch (Exception e)
            {
                Trace.WriteLine(string.Format("{0} --- Exception when GetCountry. Id = {1}, Message = (2). Stack={3}", DateTime.Now, Id, e.Message, e.StackTrace));
                throw;
            }
        }

    }
}
