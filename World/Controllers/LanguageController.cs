﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace World.Controllers
{
    public class LanguageController : ApiController
    {
        // GET: api/Language
        public Dictionary<string, string> Get()
        {
            // return all language supported, for test only.
            var lgs = new Dictionary<string,string>();
            lgs.Add("","");
            lgs.Add("de", "German");
            lgs.Add("es", "Spanish");
            lgs.Add("fr", "French");
            lgs.Add("ja", "Japanese");
            lgs.Add("it", "Italian");
            return lgs;
        }
    }
}
