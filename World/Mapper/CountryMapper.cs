﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using World.DomainModel;
using World.ViewModels;
using World.Service;

namespace World.Mapper
{
    public class CountryMapper
    {
        ICountryService _svc = new CountryService();

        public CountryViewModel MapCountryViewFrom(Country FromModel)
        {
            CountryViewModel ToModel = new CountryViewModel() {
                Id = FromModel.id,
                Capital = FromModel.capital,
                Name = FromModel.name
            };
            ToModel.Borders = (from c in _svc.GetCountries()
                               where FromModel.borders.Contains(c.code)
                                select (c.translations)).ToList();
            return ToModel;
        }

        public IList<CountryListItemModel> MapCountryListFrom(IList<Country> FromModel)
        {
            IList<CountryListItemModel> ToModel = new List<CountryListItemModel>();
            foreach (var c in FromModel)
            {
                ToModel.Add(new CountryListItemModel() { Id = c.id, Name = c.name });
            }
            return ToModel;
        }
    }
}