﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace World.ViewModels
{
    public class CountryViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Capital { get; set; }
        // border countries name with language as key.
        public List<Dictionary<string,string>> Borders { get; set; }
    }
}