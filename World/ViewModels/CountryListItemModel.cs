﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace World.ViewModels
{
    public class CountryListItemModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}