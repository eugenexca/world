﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet;
using System.Net.Http;
using World.DomainModel;
using System.Threading.Tasks;
using System.Diagnostics;

namespace World.Repository
{
    public class CountryRepository : ICountryRepository
    {
        const string baseUrl = "https://restcountries.eu/";
        const string path = "rest/v1/all";
        public async Task<IList<Country>> GetCountries()
        {
            Trace.WriteLine(string.Format("{0} --- Call external WebApi. GetCountries. url = {1}{2}", DateTime.Now, baseUrl, path));

            IList<Country> result = null;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<IList<Country>>();
            }

            ////for test only.
            //IList<Country> result = new List<Country>();
            //result.Add(new Country { id = 1, name = "af", capital = "cap city" ,borders = new string[] { "AFG", "UKR" }, translations=new Dictionary<string, string> { { "de","gm lang"} } });
            //result.Add(new Country { id = 2, name = "countryb", capital = "cap city 2", borders = new string[] { "BBC", "CCA" }, translations = new Dictionary<string, string> { { "de", "gm lang count2" } } });

            return result;
        }
    }
}