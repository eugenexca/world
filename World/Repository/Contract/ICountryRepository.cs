﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using World.DomainModel;

namespace World.Repository
{
    interface ICountryRepository
    {
        Task<IList<Country>> GetCountries();
    }
}
